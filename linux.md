## Linux Basic Commands


1. Viewing Processes with `ps` and `grep`

```
ps aux | grep { SOME TERM } 
```

The `ps` command helps you look at all the processes running on your computer.  It works a lot like the Activity Monitor on Mac or Task Manager in Windows, except there's no graphical user interface (GUI).

- `a = all processes running`
- `u = user owned processes`
- `x = processes not attached to terminal`

The `|` symbol is a pipe which takes the output of the current process and sends it to the next application waiting, so in this case, `grep`.

`grep` is a Global Regular Expression, which searches any term given to it within the output of the previous command in this context.

Altogether, `ps aux | grep rails` would search for any processes with name rails.  We could then see the tasks running and decide to kill any task if it's running slow with the `kill -9 {Process ID}` command where `{Process ID}` is the id number of the process in the output.

```
ubuntu:~/environment/docs $ ps aux | grep rails
ubuntu   22218  0.0  0.0  14856  1000 pts/2    S+   22:36   0:00 grep rails
```
^ Sample output, where 22218 is the `{Process ID}`.


2. The `curl` command, cURL

Allows one to download files from the internet via the command line.  For example the command:

```
curl -OL cdn.learnenough.com/kitten.jpg
```

will download a kitten image from the website learnenough.com.   You can specify the output folder by putting the name of the folder on your computer in front of the download link but after the `-OL` flag which just means `O` output and `L` list files downloaded. 