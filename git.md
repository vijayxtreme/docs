## Github notes

### Some notes for `git`

`git add -A` or `git add --all` to add all the files in your repo and put them up on staging. 

> Note you may not want to stage all your files, so you can just use `git add <filename>`

`git commit -m "Your Message Here"` - use this to add a helpful message (required) to tell others what changes you made.  If it helps, you can use [Semantic Versioning](https://semver.org/)'s style of messaging as a reference and guidelines for how to write helpful messaging.

`git commit -am "Your Message Here"` - use this to add and commit any files you've ALREADY tracked (as a shortcut) to writing out the previous two commands.

> Note: If you don't add new files with git add -A, they won't be tracked with the `git commit -am "Some Message"` shortcut (this only works for files already tracked)